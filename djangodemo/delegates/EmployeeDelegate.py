from django.core import exceptions
from django.core import serializers
from django.db import models
from django.db import utils

from djangodemo.models.Employee import Employee
from djangodemo.exceptions import Exceptions

 #======================================================================
# 
# Encapsulates data for model Employee
#
# @author Dev Team
#
#======================================================================

#======================================================================
# Class EmployeeDelegate Declaration
#======================================================================
class EmployeeDelegate :

#======================================================================
# Function Declarations
#======================================================================

	def get(self, employeeId ):
		try:	
			employee = Employee.objects.filter(id=employeeId)
			return employee.first();
		except Employee.DoesNotExist:
			raise ProcessingError("Employee with id " + str(employeeId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 

	def createFromJson(self, employee):
		for model in serializers.deserialize("json", employee):
			model.save()
			return model;

	def create(self, employee):
		employee.save()
		return employee;

	def saveFromJson(self, employee):
		for model in serializers.deserialize("json", employee):
			model.save()
			return employee;
	
	def save(self, employee):
		employee.save()
		return employee;
	
	def delete(self, employeeId ):
		errMsg = "Failed to delete Employee from db using id " + str(employeeId)
		
		try:
			employee = Employee.objects.get(id=employeeId)
			employee.delete()
			return True
		except Employee.DoesNotExist:
			raise ProcessingError("Employee with id " + str(employeeId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 
	
	def getAll(self):
		try:
			all = Employee.objects.all()
			return all;
		except utils.DatabaseError:
			raise StorageReadError("Failed to get all Employee from db")
		except Exception:
			return None;
		
