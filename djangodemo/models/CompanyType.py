from django.db import models
 #======================================================================
# 
# Encapsulates data for model CompanyType
#
# @author Dev Team
#
#======================================================================

#======================================================================
# Class CompanyType Declaration (enumerated type)
#======================================================================
from enum import Enum 
class CompanyType(Enum):   # A subclass of Enum
	S_Corp = 'S_Corp'
	LLC = 'LLC'
	C_Corp = 'C_Corp'
