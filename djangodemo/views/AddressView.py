import json

from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse

from djangodemo.delegates.AddressDelegate import AddressDelegate

 #======================================================================
# 
# Encapsulates data for View Address
#
# @author Dev Team
#
#======================================================================

#======================================================================
# Class AddressView function declarations
#======================================================================
def index(request):
	return HttpResponse("Hello, world. You're at the Address index.")

def get(request, addressId ):
	delegate = AddressDelegate()
	responseData = delegate.get( addressId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def create(request):
	address = json.loads(request.body)
	delegate = AddressDelegate()
	responseData = delegate.createFromJson( address )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def save(request):
	address = json.loads(request.body)
	delegate = AddressDelegate()
	responseData = delegate.save( address )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def delete(request, addressId ):
	delegate = AddressDelegate()
	responseData = delegate.delete( addressId )
	return HttpResponse(responseData, content_type="application/json");

def getAll(request):
	delegate = AddressDelegate()
	responseData = delegate.getAll()
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

